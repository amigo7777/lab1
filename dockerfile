FROM python:3

COPY task21.py /

RUN pip install psycopg2

CMD [ "python", "task21.py" ]