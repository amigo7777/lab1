import psycopg2
from psycopg2 import Error
import datetime

datestudents = []

try:
    # Подключение к существующей базе данных
    connection = psycopg2.connect(user="postgres",
                                  password="27072001qw", host="127.0.0.1",
                                  port="5432", database="dataname"
                                  )

    # Курсор для выполнения операций с базой данных
    cursor = connection.cursor()
    # Распечатать сведения о PostgreSQL
    #print("Информация о сервере PostgreSQL")
    print(connection.get_dsn_parameters(), "\n")
    # Выполнение SQL-запроса
    cursor.execute("SELECT * FROM students;")
    # Получить результат
    record = cursor.fetchall()
    datestudents = record
    #print("Результат", record)

except (Exception, Error) as error:
    print("Ошибка при работе с PostgreSQL", error)
finally:
    if connection:
        cursor.close()
        connection.close()
        print("Соединение с PostgreSQL закрыто\n")


def maxdate(arrdate):
    maxy = None
    for q in arrdate:
        for g in arrdate:
            if q != g:
                if q > g:
                    maxy = q
    return maxy


def mindate(arrdate):
    miny = datetime.date(2025, 12, 30)
    for q in arrdate:
        if q < miny:
            miny = q
    return miny


def rezult(date):
    arrdate = []
    arrfio = []
    for i in date:
        fio = f'{i[1]} {i[2]} {i[3]}'
        databirth = i[5]
        arrfio.append(fio)
        arrdate.append(databirth)

    maxd = maxdate(arrdate)
    mind = mindate(arrdate)

    for t in range(len(arrdate)):
        if arrdate[t] == maxd or arrdate[t] == mind:
            print(arrfio[t], arrdate[t])


if __name__ == '__main__':
    rezult(datestudents)
