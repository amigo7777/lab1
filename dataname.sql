--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

-- Started on 2023-01-18 17:35:09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 201 (class 1259 OID 26333)
-- Name: students; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.students (
    id integer NOT NULL,
    name character varying(30),
    lastname character varying(30),
    firstname character varying(30),
    age integer,
    datebirth date NOT NULL
);


ALTER TABLE public.students OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 26331)
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.students_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.students_id_seq OWNER TO postgres;

--
-- TOC entry 2990 (class 0 OID 0)
-- Dependencies: 200
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.students_id_seq OWNED BY public.students.id;


--
-- TOC entry 2850 (class 2604 OID 26336)
-- Name: students id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students ALTER COLUMN id SET DEFAULT nextval('public.students_id_seq'::regclass);


--
-- TOC entry 2984 (class 0 OID 26333)
-- Dependencies: 201
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (1, 'Yaroslav', 'Jestyakov', 'Alekseevith', NULL, '2000-06-20');
INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (2, 'Nikita', 'Solovyev', 'Aleksandrovith', NULL, '2000-05-10');
INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (3, 'Ignat', 'Mortian', 'Almazovith', NULL, '1999-03-12');
INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (4, 'Svetlana', 'Gracheva', 'Viktorovna', NULL, '1998-03-25');
INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (5, 'Ivan', 'Sechenov', 'Ivanovith', NULL, '2001-04-17');
INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (6, 'Alexander', 'Pomolov', 'Sergeevith', NULL, '2002-05-01');
INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (7, 'Dmitri', 'Kuznecov', 'Romanovith', NULL, '2002-05-18');
INSERT INTO public.students (id, name, lastname, firstname, age, datebirth) VALUES (8, 'Artem', 'Grishkin', 'Alekseevith', NULL, '1998-03-10');


--
-- TOC entry 2991 (class 0 OID 0)
-- Dependencies: 200
-- Name: students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.students_id_seq', 8, true);


--
-- TOC entry 2852 (class 2606 OID 26338)
-- Name: students students_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (id);


-- Completed on 2023-01-18 17:35:09

--
-- PostgreSQL database dump complete
--

